# Exercise - Model training
During model training, a predefined model should be trained and optimized. During training we would like to log all interesting metrics, model parameters, and custom tags.

Use the 'code/main.py' file as a base for the model training.

## Azure Machine Learning Utils
In order to connect to Azure Machine Learning (AML) service, use the 'code/aml_utils.py' code snippets in your training script. Use the portal and web-interface of AML to review your experiment runs.

1. Log your training runs to your personal AML experiment using the mlflow methods from the 'azure-mlflow' package.
2. Try to optimize your model by running multiple training setups. Log model metrics, tags, and parameters for each experiment run.
