# Exercise requirements

## Environment variables
Create an “.env” file and add keys, value pairs as suggested in the “.env.template” file. Make sure you use user specific names for the experiment_name, model_name, and service_name.

## Needed files
Make sure you have the following files listed in your local repo clone:
* .vscode/settings.json
* .vscode/extensions.json
* code/main.py
* code/aml_utils.py
* exercises/*.md
* .env.template
* .gitignore
* README.md
* requirements.txt
* environment.yml
* extra_deploy_requirements.txt

## Optional: Azure CLI
### Azure CLI setup
Test in your terminal if you already have the azure cli (should be >= 2.25.0):
```
az version
```
If lower version, please upgrade to above mentioned requirements using the following terminal command:
```
az upgrade
```
If not installed follow the link: https://docs.microsoft.com/en-us/cli/azure/install-azure-cli
### Azure login
If the CLI can open your default browser, it will do so and load a sign-in page. Otherwise, you need to open a browser and follow the instructions on the command line. The instructions involve browsing to https://aka.ms/devicelogin and entering an authorization code. Domain can be found in the portal (https://portal.azure.com/#settings/directory)
```
az login --tenant <DOMAIN> --allow-no-subscriptions
```
Set your active subscription by the following command in your terminal:
```
az account set -s "<YOUR_SUBSCRIPTION_NAME_OR_ID>"
```
