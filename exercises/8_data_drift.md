# Exercise - Data Drift
We recently trained our classifier model based on the infamous digits dataset. New production data has been received. We would like to know if the data used for training is still valid with the new production data that we got after running in production for a while.

## Measure data drift
- Make sure you have all required packages in `environment.yml` or `requirements.txt`
- Create a data drift report using deepchecks to check whether the new input data in `digits.parquet` has drifted compared to the reference train set. Tip: Use an iPython notebook for data exploration and visualisation, therefore notebooks are ideal for data drift analysis.
- Log relevant drift metrics to Azure ML similar to the MLFlow integration we used for model training.
- (Optional) Add a check to the FastAPI app that makes the app return an error if data drift is detected, validate that it works as intended.
- (Optional) Try to find out what caused the data to drift.
Hint: https://scikit-learn.org/stable/auto_examples/classification/plot_digits_classification.html#digits-dataset
Hint: `images = X.to_numpy().reshape(-1,8,8)`
