# Exercise - Docker
We will deploy our model locally on our laptop, as a FastAPI app.

## Validate without docker
- Run `python code/main.py` to create the `model.joblib` file
- Start the API without docker by running: 
```
uvicorn code.fastapi_app:app --host 0.0.0.0 --port 6789 --reload
```
- Validate that the API works by executing the code in `validate_fastapi_deployment.py`

## Build with docker
- Create a Dockerfile that can be used to deploy the FastAPI api
- Build the docker image
- Start a container from using your docker image (make sure the correct port is forwarded)
- Validate again that the API works by executing the code in `validate_fastapi_deployment.py`
