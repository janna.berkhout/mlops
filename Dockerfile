FROM python:3.10

WORKDIR /code

COPY ./requirements.txt /code/

RUN pip install --no-cache-dir --upgrade -r requirements.txt

COPY ./model.joblib /code/
COPY code/fastapi_app.py /code/

CMD ["uvicorn", "fastapi_app:app", "--host", "0.0.0.0", "--port", "6789"]
