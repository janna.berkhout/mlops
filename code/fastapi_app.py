import joblib
import pandas as pd
from fastapi import FastAPI
from pydantic import BaseModel
from xgboost.sklearn import XGBClassifier

model: XGBClassifier = joblib.load("model.joblib")
app = FastAPI()


class DigitDataset(BaseModel):
    """Dataset of digits, datatypes are validated for each request."""

    data: list[dict[str, float]]


@app.post("/score")
async def score(dataset: DigitDataset):
    """Create a prediction"""
    df = pd.DataFrame(dataset.data)
    y_pred = model.predict(df)
    return y_pred.tolist()
