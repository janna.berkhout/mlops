# Use below code to connect to Azure after authentication in the terminal CLI with "az login".
# Workspace will be loaded to create an interface between your code and the AML workspace.
# Mlflow will use the track_uri method to connect to AML, now your able to use mlflow endpoint to track, log and register training runs and models.
import os

import mlflow
from azureml.core import Workspace, authentication
from dotenv import load_dotenv


def connect():
    """Set up connection to Azure ML MLFlow"""
    load_dotenv()
    # User authentication
    interactive_auth = authentication.InteractiveLoginAuthentication(
        tenant_id=os.environ["TENANT_ID"]
    )

    # Get workspace environment
    ws = Workspace.get(
        name=os.environ["WORKSPACE_NAME"],
        auth=interactive_auth,
        subscription_id=os.environ["SUBSCRIPTION_ID"],
        resource_group=os.environ["RESOURCE_GROUP"],
    )

    # Setup mlflow tracking to Azure Machine Learning workspace
    mlflow.set_tracking_uri(ws.get_mlflow_tracking_uri())
